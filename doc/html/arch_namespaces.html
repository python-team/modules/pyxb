<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <title>Namespaces &#8212; PyXB 1.2.6 documentation</title>
    
    <link rel="stylesheet" href="_static/classic.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '1.2.6',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true,
        SOURCELINK_SUFFIX: '.txt'
      };
    </script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Component Model" href="arch_component.html" />
    <link rel="prev" title="Architecture" href="architecture.html" /> 
  </head>
  <body role="document">
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="arch_component.html" title="Component Model"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="architecture.html" title="Architecture"
             accesskey="P">previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">PyXB 1.2.6 documentation</a> &#187;</li>
          <li class="nav-item nav-item-1"><a href="architecture.html" accesskey="U">Architecture</a> &#187;</li>
    <li style="margin-left: 20px">PyXB hosted on <a href="http://sourceforge.net/projects/pyxb"><img src="http://sflogo.sourceforge.net/sflogo.php?group_id=263147&amp;type=9"
    width="80" height="15" alt="Get PyXB: Python XML Schema Bindings at SourceForge.net. Fast, secure and Free Open Source software downloads"/></a></li>
     

      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="namespaces">
<span id="id1"></span><h1>Namespaces<a class="headerlink" href="#namespaces" title="Permalink to this headline">¶</a></h1>
<p>The most important concept to understand when working with complex XML
documents is the <a class="reference external" href="http://www.w3.org/TR/REC-xml-names/">namespace</a>.  A
namespace is nothing more than a map from names to objects, partitioned into
groups within which the names must uniquely identify an object.</p>
<p>A namespace is identified by its name, which is a <a class="reference external" href="http://www.rfc-editor.org/rfc/rfc3986.txt">URI</a>.  Although it is common to use
URIs like <code class="docutils literal"><span class="pre">http://www.w3.org/2001/XMLSchema</span></code> as namespace names, the name
is simply an opaque identifier: it does not have to resolve to a Web site or
anything helpful.  <code class="docutils literal"><span class="pre">dinner:ParsnipsOnTuesday</span></code> is a perfectly valid
namespace name.</p>
<p>Equally, namespaces and XML schemas are not the same thing.  A schema is
simply a mechanism for specifying the contents of a namespace.  It is common
to use the <code class="docutils literal"><span class="pre">include</span></code> directive in XMLSchema to combine multiple schema
into a single namespace.  It is less common, though equally valid, to use
<code class="docutils literal"><span class="pre">xmlns</span></code> or <code class="docutils literal"><span class="pre">xs:schemaLocation</span></code> to select alternative schemas to use for
the same namespace in different instance documents, as in the <a class="reference external" href="http://www.xfront.com/VariableContentContainers.html">dangling type</a> pattern.</p>
<p>This diagram shows the class structure of the PyXB namespace infrastructure.
The central object is the <a class="reference internal" href="pyxb.namespace.html#pyxb.namespace.Namespace" title="pyxb.namespace.Namespace"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.Namespace</span></code></a>.  Four mix-in
classes provide implementations of separate namespace functions.
<a class="reference internal" href="pyxb.namespace.html#pyxb.namespace.ExpandedName" title="pyxb.namespace.ExpandedName"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.ExpandedName</span></code></a> is used ubiquitously to pair local names
with their namespaces.  <a class="reference internal" href="pyxb.namespace.html#pyxb.namespace.NamedObjectMap" title="pyxb.namespace.NamedObjectMap"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.NamedObjectMap</span></code></a> maps names to the objects
(generally, schema components) for a particular category of object.  The
<code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.NamespaceContext</span></code> class provides information related to
the use of namespaces in XML documents, including mappings from prefixes to
namespaces.</p>
<img alt="_images/NamespaceCore.jpg" src="_images/NamespaceCore.jpg" />
<div class="section" id="namespace-category-maps">
<h2>Namespace Category Maps<a class="headerlink" href="#namespace-category-maps" title="Permalink to this headline">¶</a></h2>
<p>The <a class="reference internal" href="pyxb.namespace.html#pyxb.namespace._NamespaceCategory_mixin" title="pyxb.namespace._NamespaceCategory_mixin"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace._NamespaceCategory_mixin</span></code></a> provides support for
discrete categories of named objects.  It allows arbitrary,
runtime-identified, groups of objects to be registered in individual
dictionaries within the namespace.  For example, XML Schema require that type
definitions, element declarations, and attribute declarations be distinct
categories of named objects in a namespace.  PyXB also maintains separate
categories for attribute groups, model groups, identity constraint
definitions, and notation declarations, which also must be unique within their
category.</p>
<p>Other groups of objects can be stored in a namespace.  For example, the WSDL
definition of a service may choose to use the same namespace name for its
types as for its definitions, adding services, ports, messages, bindings,
and portTypes as named objects that can be identified.</p>
</div>
<div class="section" id="namespace-component-associations">
<h2>Namespace Component Associations<a class="headerlink" href="#namespace-component-associations" title="Permalink to this headline">¶</a></h2>
<p>The <a class="reference internal" href="pyxb.namespace.html#pyxb.namespace._NamespaceComponentAssociation_mixin" title="pyxb.namespace._NamespaceComponentAssociation_mixin"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace._NamespaceComponentAssociation_mixin</span></code></a> provides
support for associating schema components with a namespace.  Of particular
interest is that a namespace can be comprised of components defined from
multiple sources (generally, distinct schema documents).  In addition, there
are anonymous components (e.g., complex types defined within element members
of complex types) which are implicitly associated with the namespace although
they cannot be named within it.  These must all be stored within the namespace
so that a complete set of bindings can be generated in a single Python module.</p>
</div>
<div class="section" id="namespace-resolution">
<span id="resolution"></span><h2>Namespace Resolution<a class="headerlink" href="#namespace-resolution" title="Permalink to this headline">¶</a></h2>
<p>Named objects are often associated with namespaces through XML elements in a
document.  For example:</p>
<div class="highlight-default"><div class="highlight"><pre><span></span><span class="o">&lt;</span><span class="n">xs</span><span class="p">:</span><span class="n">attribute</span> <span class="n">xmlns</span><span class="p">:</span><span class="n">xs</span><span class="o">=</span><span class="s2">&quot;http://www.w3.org/2001/XMLSchema&quot;</span>
 <span class="n">name</span><span class="o">=</span><span class="s2">&quot;vegetable&quot;</span> <span class="nb">type</span><span class="o">=</span><span class="s2">&quot;xs:string&quot;</span> <span class="n">default</span><span class="o">=</span><span class="s2">&quot;parsnip&quot;</span><span class="o">/&gt;</span>
</pre></div>
</div>
<p>specifies an attribute declaration.  In turn, references to names appear
within XML elements, usually as values of specific attributes.  The <code class="docutils literal"><span class="pre">type</span></code>
portion of the attribute declaration above also identifies an object by name,
and it must be possible to resolve the named object.  The work involved in
associating names with schema components is encapsulated in the
<a class="reference internal" href="pyxb.namespace.html#pyxb.namespace.resolution._NamespaceResolution_mixin" title="pyxb.namespace.resolution._NamespaceResolution_mixin"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.resolution._NamespaceResolution_mixin</span></code></a> class.</p>
<p>The following <a class="reference external" href="http://www.w3.org/TR/REC-xml-names/#concepts">concepts</a> are
important to understand:</p>
<ul class="simple">
<li>An <a class="reference external" href="http://www.w3.org/TR/xmlschema-2/#NCName">NCName</a> (&#8220;no-colon name&#8221;)
is an identifier, specifically one without any colon (&#8221;:&#8221;) characters,
serving as a local name.</li>
<li>A <a class="reference external" href="http://www.w3.org/TR/xmlschema-2/#QName">QName</a> (&#8220;qualified name&#8221;) is
an local name with an optional prefix, separated from it by a colon, which
identifies a context for the local name.</li>
<li>The prefix is mapped using <a class="reference external" href="http://www.w3.org/TR/REC-xml-names/#ns-decl">xmlns</a> attributes to a namespace
name, which is a URI.</li>
<li>The combination of a namespace URI and the local name comprise an <a class="reference external" href="http://www.w3.org/TR/REC-xml-names/#dt-expname">expanded
namespace name</a>, which is
represented by <a class="reference internal" href="pyxb.namespace.html#pyxb.namespace.ExpandedName" title="pyxb.namespace.ExpandedName"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.ExpandedName</span></code></a>.</li>
<li>The category within which the local name must be resolved in the namespace
is determined through external information, in the above case the fact of
the QName&#8217;s appearance in a <code class="docutils literal"><span class="pre">type</span></code> attribute in an <code class="docutils literal"><span class="pre">attribute</span></code>
declaration of an XML schema.</li>
</ul>
<p><a class="reference internal" href="pyxb.namespace.html#pyxb.namespace._NamespaceCategory_mixin" title="pyxb.namespace._NamespaceCategory_mixin"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace._NamespaceCategory_mixin</span></code></a> is used to define the set of
categories supported by a namespace and to add named objects to those
categories.  A name is <strong>resolved</strong> when the object with which it is
associated has been identified.  Objects are <strong>resolved</strong> when any names on
which they depend have been resolved.
<a class="reference internal" href="pyxb.namespace.html#pyxb.namespace.resolution._NamespaceResolution_mixin" title="pyxb.namespace.resolution._NamespaceResolution_mixin"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.resolution._NamespaceResolution_mixin</span></code></a> provides a mechanism to
hold on to names that have been encountered but whose associated objects
have not yet been resolved (perhaps because the named object on which they
depend has not been defined).</p>
<p>Because one named object (e.g., a model group definition) might require
resolution of another (e.g., an element reference), resolution is an iterative
process, implemented by
<a class="reference internal" href="pyxb.namespace.html#pyxb.namespace.resolution._NamespaceResolution_mixin.resolveDefinitions" title="pyxb.namespace.resolution._NamespaceResolution_mixin.resolveDefinitions"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.resolution._NamespaceResolution_mixin.resolveDefinitions</span></code></a>,
and executed when all named objects have been added to the namespace.  It
depends on <a class="reference internal" href="pyxb.namespace.html#pyxb.namespace.resolution.NamespaceContext" title="pyxb.namespace.resolution.NamespaceContext"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.resolution.NamespaceContext</span></code></a> to identify named
objects using the
<a class="reference internal" href="pyxb.namespace.html#pyxb.namespace.resolution.NamespaceContext.interpretQName" title="pyxb.namespace.resolution.NamespaceContext.interpretQName"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.resolution.NamespaceContext.interpretQName</span></code></a> method.</p>
</div>
<div class="section" id="expanded-names">
<h2>Expanded Names<a class="headerlink" href="#expanded-names" title="Permalink to this headline">¶</a></h2>
<p>An <a class="reference internal" href="pyxb.namespace.html#pyxb.namespace.ExpandedName" title="pyxb.namespace.ExpandedName"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.ExpandedName</span></code></a> instance couples a local name with
(optionally) a namespace, resulting in a QName.  This class also integrates
with namespace categories, permitting lookup of the object with its name in a
specific category by using the category name as a method.  For example, the
following two expressions are equivalent:</p>
<div class="highlight-default"><div class="highlight"><pre><span></span><span class="c1"># Short-hand method</span>
<span class="n">en</span><span class="o">.</span><span class="n">typeDefinition</span><span class="p">()</span>
<span class="c1"># Detailed equivalent</span>
<span class="n">en</span><span class="o">.</span><span class="n">namespace</span><span class="p">()</span><span class="o">.</span><span class="n">categoryMap</span><span class="p">(</span><span class="s1">&#39;typeDefinition&#39;</span><span class="p">)</span><span class="o">.</span><span class="n">get</span><span class="p">(</span><span class="n">en</span><span class="o">.</span><span class="n">localName</span><span class="p">())</span>
</pre></div>
</div>
<p>Both produce the type definition with the given name, or <code class="docutils literal"><span class="pre">None</span></code> if there is
no such definition.  The short-hand method interface works for any category
defined within the expanded name&#8217;s namespace; it is not limited to the
standard set of component categories.</p>
<p>Methods are also present to test whether the name matches a DOM node, and to
retrieve the named attribute (if present) from a DOM node.</p>
<p>In this version of PyXB, the hash codes and comparison methods for
<a class="reference internal" href="pyxb.namespace.html#pyxb.namespace.ExpandedName" title="pyxb.namespace.ExpandedName"><code class="xref py py-obj docutils literal"><span class="pre">ExpandedName</span></code></a> have been overridden so that
an expanded name with no namespace is treated equivalently to the string value
of the local name.  This simplified management of default namespace lookups in
earlier versions of PyXB, but may no longer be necessary; reliance on this
feature is discouraged.</p>
</div>
<div class="section" id="namespace-context">
<h2>Namespace Context<a class="headerlink" href="#namespace-context" title="Permalink to this headline">¶</a></h2>
<p><a class="reference external" href="http://www.w3.org/TR/REC-xml-names/">Namespaces in XML</a> specifies how the
<code class="docutils literal"><span class="pre">xmlns</span></code> attributes are used to associate prefix strings with namespaces.
The <code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.NamespaceContext</span></code> class supports this by associating
with each node in a DOM document the contextual information extracted from
<code class="docutils literal"><span class="pre">xmlns</span></code> and other namespace-relevant attributes.</p>
<p>The namespace context consists of three main parts:</p>
<ul class="simple">
<li>The <a class="reference external" href="http://www.w3.org/TR/REC-xml-names/#defaulting">default namespace</a>
specifies the namespace in which unqualified names are resolved.</li>
<li>The <a class="reference external" href="http://www.w3.org/TR/xmlschema-1/#key-targetNS">target namespace</a>
is the namespace into which new name-to-component associations will be
recorded.</li>
<li>The <a class="reference external" href="http://www.w3.org/TR/REC-xml-names/#scoping">in-scope namespaces</a>
of a DOM node are those which can be identified by a prefix applied to
names that appear in the node.</li>
</ul>
<p>Methods are provided to define context on a per-node basis within a DOM
structure, or to dynamically generate contexts based on parent contexts and
local namespace declarations as needed when using the SAX parser.</p>
</div>
<div class="section" id="other-concepts">
<h2>Other Concepts<a class="headerlink" href="#other-concepts" title="Permalink to this headline">¶</a></h2>
<div class="section" id="absent-namespaces">
<span id="absentnamespaces"></span><span id="index-0"></span><h3>Absent Namespaces<a class="headerlink" href="#absent-namespaces" title="Permalink to this headline">¶</a></h3>
<p>Some schemas fail to specify a default namespace, a target namespace, or
both.  These cases are described by the term &#8220;absent namespace&#8221;; sometimes
it is said that an object for which the target namespace is absent is in &#8220;no
namespace&#8221;.</p>
<p>If the target namespace for a schema is absent, we still need to be able to
store things somewhere, so we represent the target namespace as a normal
<a class="reference internal" href="pyxb.namespace.html#pyxb.namespace.Namespace" title="pyxb.namespace.Namespace"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.Namespace</span></code></a> instance, except that the associated URI is
<code class="docutils literal"><span class="pre">None</span></code>.  If in the same schema there is no default namespace, the default
namespace is assigned to be this absent (but valid) target namespace, so that
QName resolution works.  Absence of a target namespace is the only situation
in which resolution can succeed without some sort of namespace declaration.</p>
<p>The main effect of this is that some external handle on the Namespace instance
must be retained, because the namespace cannot be identified in other
contexts.  PyXB supports this by defining a <code class="docutils literal"><span class="pre">Namespace</span></code> variable within each
binding module, thus allowing access to the namespace instance via syntax like
<code class="docutils literal"><span class="pre">pyxb.bundles.wssplat.wsdl11.Namespace</span></code>.</p>
</div>
</div>
<div class="section" id="storage-of-namespaces">
<span id="namespacestorage"></span><h2>Storage of Namespaces<a class="headerlink" href="#storage-of-namespaces" title="Permalink to this headline">¶</a></h2>
<p>In PyXB, when the <a class="reference internal" href="arch_component.html#componentmodel"><span class="std std-ref">Component Model</span></a> is used to define various elements,
attributes, and types by representing them in Python instances, those instance
objects are stored in a <a class="reference internal" href="pyxb.namespace.html#pyxb.namespace.Namespace" title="pyxb.namespace.Namespace"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.Namespace</span></code></a> instance.  In addition
to generating code corresponding to those objects, it is possible to save the
pre-computed objects into a file so that they can be referenced in other
namespaces.</p>
<p>PyXB uses the Python pickling infrastructure to store the namespace component
model into a file in the same directory as the generated binding, but with a
suffix <code class="docutils literal"><span class="pre">.wxs</span></code>.  When a schema is processed that refers to a namespace, the
serialized component model for the namespace is read in so that the referring
namespace can resolve types in it.</p>
<div class="section" id="the-namespace-archive-model">
<span id="namespace-archive"></span><h3>The Namespace Archive Model<a class="headerlink" href="#the-namespace-archive-model" title="Permalink to this headline">¶</a></h3>
<p>Recall that the contents of a namespace can be defined from multiple sources.
While in the simplest cases the namespace is defined by combining components
from one or more schemas, the set of schemas that define a namespace may be
different for different documents.  One way this is used is the <cite>dangling
types&lt;http://www.xfront.com/VariableContentContainers.html#method4&gt;</cite> pattern.</p>
<p>Another not uncommon situation is to use a namespace <cite>profile</cite>, which is a
subset of the full namespace intended for use in a particular application.
For example, the Geography Markup Language defines three profiles denoted
&#8220;GML-SF&#8221; for &#8220;simple features&#8221;; these profiles do not include the more complex
structures that are needed for unusual situations.</p>
<p>Finally, some use cases require that a namespace be extended with
application-specific information provided in a schema that adds to rather than
replaces the base namespace definition.  As with profiles these extensions
should be provided in separate Python modules, but unlike profiles the
original binding must be imported separately to provide the application&#8217;s
perspective of the full namespace content.</p>
<p>To support these cases, PyXB must:</p>
<ul class="simple">
<li>Support distinct Python binding modules for one namespace (e.g., dangling
type implementations and profiles)</li>
<li>Support shared Python binding modules for one namespace (e.g., a base
namespace with application-specific extensions)</li>
<li>Ensure the component model for each binding module is retained within a
single archive regardless of whether that component model is complete for
the module&#8217;s namespace</li>
</ul>
<p>Naive management of these multiple information sources will cause havoc, since
namespaces do not allow multiple objects to share the same name.</p>
<p>The relations of the various classes involved in storing namespace data are
depicted in the following diagram:</p>
<img alt="_images/NamespaceArchive.jpg" src="_images/NamespaceArchive.jpg" />
<p>The namespace archive facility must support the following situations:</p>
<ul class="simple">
<li>The archive stores the complete set of components for a single namespace
(most common)</li>
<li>The archive stores components from multiple namespaces which are
interdependent, but together completely define the expected contents of the
namespaces</li>
<li>The archive stores a complete subset of the standard components of a
namespace (the <cite>profile</cite> situation)</li>
<li>The archive extends a namespace with additional components, often required
for a particular application.  It is usually necessary to read another
archive to determine the full namespace content.</li>
</ul>
<p>Because of interdependencies between namespaces stored in a single archive,
archives are read as complete entities: i.e., from a single archive you cannot
read the components corresponding to one namespace while ignoring those from
another.</p>
<p>The component model for a namespace is read from a namespace archive only when
it is necessary to generate new bindings for a namespace that refers to it,
through import or namespace declarations.  The component model is defined by
invoking the <a class="reference internal" href="pyxb.namespace.html#pyxb.namespace.Namespace.validateComponentModel" title="pyxb.namespace.Namespace.validateComponentModel"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.Namespace.validateComponentModel</span></code></a> method.</p>
<p>Within an archive, each namespace can be marked as <cite>private</cite> or <cite>public</cite>.
When the component model for a namespace is validated, all archives in which
that namespace is present and marked <cite>public</cite> are read and integrated into the
available component models.</p>
<p>When an archive is read, namespaces in it that are marked <cite>private</cite> are also
integrated into the component model.  Prior to this integration, the namespace
component model is validated, potentially requiring the load of other archives
in which the namespace is marked <cite>public</cite>.</p>
<p>The contents of the namespace archive are:</p>
<ul class="simple">
<li>A set of <code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.archive._ModuleRecord</span></code> instances which
identify namespaces and mark whether they are public or private in the
archive.  Each instance in turn contains (for namespace <code class="docutils literal"><span class="pre">A</span></code>):<ul>
<li>the set of <a class="reference internal" href="pyxb.namespace.html#pyxb.namespace.archive._ObjectOrigin" title="pyxb.namespace.archive._ObjectOrigin"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.archive._ObjectOrigin</span></code></a> instances which
identify the origins for components that are defined in the archive.  In
turn, each of these origins identifies by category and name the objects
that were defined by this origin and consequently are stored in the
containing archive.  Due to use of the <a class="reference external" href="http://www.w3.org/TR/xmlschema-1/#compound-schema">include</a> directive, multiple
origins may be associated with a single module record</li>
<li>the set of <a class="reference internal" href="pyxb.namespace.html#pyxb.namespace.Namespace" title="pyxb.namespace.Namespace"><code class="xref py py-obj docutils literal"><span class="pre">pyxb.namespace.Namespace</span></code></a> instances that were referenced
by <code class="docutils literal"><span class="pre">A</span></code>.  This includes namespaces that were imported into one of the
origin objects, as well as those that were incorporated simply through
reference to an object in a declared namespace</li>
</ul>
</li>
<li>The objects within the namespace that were defined by the various origins</li>
</ul>
<p>In addition to the raw component model, the namespace archive includes the
names of the Python modules into which bindings for each namespace were
generated.</p>
</div>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <h3><a href="index.html">Table Of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Namespaces</a><ul>
<li><a class="reference internal" href="#namespace-category-maps">Namespace Category Maps</a></li>
<li><a class="reference internal" href="#namespace-component-associations">Namespace Component Associations</a></li>
<li><a class="reference internal" href="#namespace-resolution">Namespace Resolution</a></li>
<li><a class="reference internal" href="#expanded-names">Expanded Names</a></li>
<li><a class="reference internal" href="#namespace-context">Namespace Context</a></li>
<li><a class="reference internal" href="#other-concepts">Other Concepts</a><ul>
<li><a class="reference internal" href="#absent-namespaces">Absent Namespaces</a></li>
</ul>
</li>
<li><a class="reference internal" href="#storage-of-namespaces">Storage of Namespaces</a><ul>
<li><a class="reference internal" href="#the-namespace-archive-model">The Namespace Archive Model</a></li>
</ul>
</li>
</ul>
</li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="architecture.html"
                        title="previous chapter">Architecture</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="arch_component.html"
                        title="next chapter">Component Model</a></p>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="_sources/arch_namespaces.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3>Quick search</h3>
    <form class="search" action="search.html" method="get">
      <div><input type="text" name="q" /></div>
      <div><input type="submit" value="Go" /></div>
      <input type="hidden" name="check_keywords" value="yes" />
      <input type="hidden" name="area" value="default" />
    </form>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="arch_component.html" title="Component Model"
             >next</a> |</li>
        <li class="right" >
          <a href="architecture.html" title="Architecture"
             >previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">PyXB 1.2.6 documentation</a> &#187;</li>
          <li class="nav-item nav-item-1"><a href="architecture.html" >Architecture</a> &#187;</li>
    <li style="margin-left: 20px">PyXB hosted on <a href="http://sourceforge.net/projects/pyxb"><img src="http://sflogo.sourceforge.net/sflogo.php?group_id=263147&amp;type=9"
    width="80" height="15" alt="Get PyXB: Python XML Schema Bindings at SourceForge.net. Fast, secure and Free Open Source software downloads"/></a></li>
     

      </ul>
    </div>
    <div class="footer" role="contentinfo">
        &#169; Copyright 2009-2017, Peter A. Bigot.
      Created using <a href="http://sphinx-doc.org/">Sphinx</a> 1.5.5.
    </div>
  </body>
</html>